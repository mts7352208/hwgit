
public class inShop {
    int items;         //кол-во товара
    double price;      //цена
    double discount;   //скидка
     public inShop (int items, double price, double discount){
       this.items=items;
       this.price=price;
       this.discount=discount;
    }
    public void cashbox(){
         double total=items*price;
         double totaldis=total-((total*discount)/100);
         System.out.print("Total:");
         System.out.printf("%.2f", total);
         System.out.println();
         System.out.print("Total with discount:");
         System.out.printf("%.2f", totaldis);
         System.out.println();

    }

}
